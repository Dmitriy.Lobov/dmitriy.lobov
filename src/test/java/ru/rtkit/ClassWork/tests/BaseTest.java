package ru.rtkit.ClassWork.tests;

import io.restassured.specification.ResponseSpecification;
import ru.rtkit.ClassWork.apiHelper.PetstoreApi;

import static io.restassured.RestAssured.expect;

public class BaseTest {

    public static ResponseSpecification resp200 = expect().statusCode(200);

    public static PetstoreApi api;

    static {

        api = new PetstoreApi();
    }

}
