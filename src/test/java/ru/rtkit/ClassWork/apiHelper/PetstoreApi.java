package ru.rtkit.ClassWork.apiHelper;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.internal.RestAssuredResponseOptionsGroovyImpl;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.given;

public class PetstoreApi {
    private final String URI = "https://petstore.swagger.io/v2";

    public PetstoreApi() {
        RestAssured.baseURI = URI;
        RestAssured.defaultParser = Parser.JSON;
//        ru.rtkit.homework.RestAssured.filters(new AllureRestAssured());
    }

    public Response get(String endpoint, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }

    public Response get(String endpoint, ResponseSpecification resp, Object... pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint, pathParams);
    }

}
