package ru.rtkit.HomeWork.apiHelper;

public class Endpoints {

    //pet
    public static final String NEW_PET = "/pet";
    public static final String PET_ID = "/pet/{petId}";

    //store
    public static final String ORDER = "/store/order";
    public static final String ORDER_ID = "/store/order/{orderId}";
}
