package ru.rtkit.HomeWork;

import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@DisplayName("Изучение RestAssured")
public class RestAssured {
    @Test
    @DisplayName("ДЗ 2")
    @Description("Получение данных питомца")
    void getPet() {
        given()
                .when()
//передача параметра в метод
                .pathParam("petId", 361)
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .log().all()
                .statusCode(200);
    }


    @Test
    @DisplayName("ДЗ 2 (создание)")
    @Description("Создание питомца")
    @Attachment
    void postNewPet() {
// формируем тело запроса, в строковом формате
        String body = "{\n" +
                " \"id\": 1,\n" +
                " \"name\": \"Test\",\n" +
                " \"status\": \"available\",\n" +
                "}";
// формируем тело запроса с использованием объекта JSONObject
        JSONObject bodyJO = new JSONObject()
                .put("id", 361)
                .put("name", "Busya")
                .put("status", "available");
        given()
                .when()
//определяем header Content-Type, т.к шлем json
                .contentType(ContentType.JSON)
//указываем тело запроса
                .body(bodyJO.toString())
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200);
    }


    @Test
    @DisplayName("ДЗ 2 (обновление)")
    @Description("Обновление данных питомца")
    @Attachment
    void updatePet() {
        String dogsName = "Муся";
        JSONObject bodyJO = new JSONObject()
                .put("id", 777)
                .put("name", dogsName)
                .put("status", "available");
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(bodyJO.toString())
                .put("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200);
    }

    @Test
    @DisplayName("ДЗ 2 (удаление)")
    @Description("Удаление питомца")
    @Attachment
    void deleteStoreOrder() {
        given()
                .when()
                .pathParam("petId", 777)
                .delete("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .statusCode(200);
    }


}