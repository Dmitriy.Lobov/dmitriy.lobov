package ru.rtkit.HomeWork;

import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@DisplayName("Изучение Hamcrest и Junit5")
public class HamcrestDemo {

    @Test
    @DisplayName("ДЗ 4.1")
    @Description("Параметризация тестов, валидация полей")
    @Attachment
    void checkPets() {
        given()
                .when()
                .pathParam("id", "name")
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .assertThat()
                //Матчер на присутствия значения для поля
                .body("name", Matchers.notNullValue());
/*
                //Попытка проверить на латиницу и кириллицу
                .body("name", Matchers.equalTo(UnicodeBlock.CYRILLIC))
                .body("name", Matchers.equalTo(UnicodeBlock.BASIC_LATIN));
        */
    }


    @ParameterizedTest
    @DisplayName("Проверка наличия созданного питомца по {id} и {name}")
    @Description("Проверка каждого из 8 созданных питомцев")
    @MethodSource("createArgs")
    @Attachment
    void checkPets(int id, String name) {
        JSONObject bodyJO = new JSONObject()
                .put("id", id)
                .put("name", name)
                .put("status", "available");
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(bodyJO.toString())
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .log().all()
                .statusCode(200);
    }

    static Stream<Arguments> createArgs() {
        return Stream.of(
                arguments(361, "Busya"),
                arguments(362, "BUSYA"),
                arguments(363, "busya"),
                arguments(364, "BuSyA"),
                arguments(365, "Буся"),
                arguments(366, "БУСЯ"),
                arguments(367, "буся"),
                arguments(368, "БуСя")
        );
    }

    //======================================================================================================================

    @Test
    @DisplayName("ДЗ 4.2")
    @Description("Использование Junit ассертов")
    @Attachment
    void postNewOrder() {
        JSONObject bodyJO = new JSONObject()
                .put("id", 5)
                .put("petId", 361)
                .put("quantity", 1)
                .put("shipDate", "2022-04-11T04:23:19.390Z")
                .put("status", "placed")
                .put("complete", true);
        given()
                .when()
                .log().all()
                .body(bodyJO.toString())
                .contentType(ContentType.JSON)
                .post("https://petstore.swagger.io/v2/store/order/")
                .then()
                .log().all()
                .statusCode(200);
        given()
                .when()
                .pathParam("orderId", bodyJO.getInt("id"))
                .get("https://petstore.swagger.io/v2/store/order/{orderId}")
                .then()
                .log().all()
                .statusCode(200)
                .assertThat()

                //значения поля petId соответствует созданному id питомца ранее
                .body("petId", Matchers.equalTo(bodyJO.getInt("petId")))

                //значения поля status соответствует одному из значений [ placed, approved, delivered ]
                .body("status", Matchers.anyOf(
                        Matchers.equalTo("placed"),
                        Matchers.equalTo("approved"),
                        Matchers.equalTo("delivered")))

                //значения поля complete соответствует типу boolean
                .body("complete", Matchers.instanceOf(Boolean.class))

                //значения поля shipDate не пустое(имеет какое-либо значение)
                .body("shipDate", Matchers.notNullValue());
    }

//======================================================================================================================

    //Работа с вложенными полями (ДЗ 4.3)
    @Test
    @DisplayName("ДЗ 4.3")
    @Description("работа с JSONObject (вложенными полями)")
    @Attachment
    void postNewPetty() {
        String body = "{\n" +
                "  \"id\": 1,\n" +
                "  \"category\": {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"Test\"\n" +
                "  },\n" +
                "  \"name\": \"Test\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"string\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"available\"\n" +
                "}";
        JSONObject bodyJO = new JSONObject()
                .put("id", 3663)
                .put("category", new JSONObject()
                        .put("id", 1)
                        .put("name", "Вложик"))
                .put("name", "Вложик")
                .put("photoUrls", new JSONArray()
                        .put("Вложик"))
                .put("tags", new JSONArray()
                        .put(new JSONObject()
                                .put("id", 1)
                                .put("name", "Вложик"))
                )
                .put("status", "available");
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(bodyJO.toString())
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .log().all()
                .statusCode(200);
    }

    //Practice =========================================================================================================
    @Test
    @DisplayName("ДЗ 4.1 (2 вариант)")
    @Description("Параметризация тестов, валидация полей")
    @Attachment
    void checkPetName() {
        String catsName = "Буся";

        JSONObject bodyJ1 = new JSONObject()
                .put("id", 25)
                .put("name", catsName)
                .put("status", "available");

        given()
                .when()
                .body(bodyJ1.toString())
                .contentType(ContentType.JSON)
                .log().all()
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200);
        given()
                .when()
                .pathParam("petId", 25)
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .log().all()
                .statusCode(200)
                .assertThat()
                .body("name", Matchers.anyOf(
                        Matchers.containsStringIgnoringCase("Буся"),
                        Matchers.containsStringIgnoringCase("Busya")));

    }


    @Test
    @DisplayName("ДЗ 4.2 (2 вариант)")
    @Description("Использование Junit ассертов")
    @Attachment
    void CheckNewOrderParams() {
        JSONObject bodyJO_ex = new JSONObject()
                .put("id", 5)
                .put("petId", 111)
                .put("quantity", 0)
                .put("shipDate", "2022-04-11T04:23:19.390Z")
                .put("status", "placed")
                .put("complete", true);
        given()
                .when()
                .log().all()
                .body(bodyJO_ex.toString())
                .contentType(ContentType.JSON)
                .post("https://petstore.swagger.io/v2/store/order/")
                .then()
                .log().all()
                .statusCode(200);
        Response OrderResponce = given()
                .when()
                .log().all()
                .pathParam("id", 5)
                .get("https://petstore.swagger.io/v2/store/order/{id}")
                .then()
                .contentType(ContentType.JSON)
                .extract()
                .response();
        Boolean StatusComplete = OrderResponce.path("complete");
        Integer OrderID = OrderResponce.path("id");
        Integer AnimalID = OrderResponce.path("petId");
        String OrderStatus = OrderResponce.path("status");
        String OrderDate = OrderResponce.path("shipDate");
        Integer OrderQuantity = OrderResponce.path("quantity");
        assertEquals(111, AnimalID);
        assertNotNull(OrderDate);
        assertTrue(StatusComplete instanceof Boolean);
        assertInstanceOf(Boolean.class, StatusComplete);
        assertThat(OrderStatus, Matchers.anyOf(
                Matchers.equalTo("placed"),
                Matchers.equalTo("approved"),
                Matchers.equalTo("delivered")));

        //Альтернатива - через список
        List<String> ValueList = new ArrayList<String>();
        ValueList.add("placed");
        ValueList.add("approved");
        ValueList.add("delivered");
        assertTrue(ValueList.contains(OrderStatus));

        //Еще альтернатива
        assertTrue(Arrays.asList("placed", "approved", "delivered").contains(OrderStatus));

        //Альтернативный вариант решения
        OrderResponce.getBody().prettyPrint();
        JsonPath MyResponseJSON = OrderResponce.getBody().jsonPath();
        assertEquals(5, MyResponseJSON.getInt("id"));

        JsonPath OrderResponce_2 = given()
                .when()
                .log().all()
                .pathParam("id", 5)
                .get("https://petstore.swagger.io/v2/store/order/{id}")
                .then()
                .contentType(ContentType.JSON)
                .extract()
                .jsonPath();
    }


}