package ru.rtkit.HomeWork;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class Database {
    String pathToDB =
            getClass().getClassLoader().getResource("sqlite/chinook.db").getPath();
    String dbUrl = "jdbc:sqlite:" + pathToDB;
    @Step("Получение данных из БД")
    private List<String> getAllPlaylists() {
        String query = "SELECT * FROM Playlists";
        List<String> playlists = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                playlists.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return playlists;
    }

    private void insertNewPlaylist(String playlist) {
        String query = "INSERT INTO Playlists(Name) VALUES(?)";
        try (Connection conn = DriverManager.getConnection(dbUrl)) {
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, playlist);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private void deletePlaylist(String playlist) {
        String query = "DELETE FROM Playlists WHERE Name=?";
        try (Connection conn = DriverManager.getConnection(dbUrl)) {
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, playlist);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @DisplayName("ДЗ 6.1")
    @Description("Выбрать другую таблицу (не genres) в базе данных и вывести в консоль любую запись, но\n" +
            "только одну")
    @Test
    void get1() {
        String query = "SELECT * FROM Playlists WHERE PlaylistId=18";
        List<String> playlists = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                playlists.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println(playlists);
    }

    @Test
    @DisplayName("ДЗ 6.2")
    @Description("Вывести все записи с таблицы tracks, где в качестве жанра выбран \"Pop\".")
    void get2() {
        String query = "SELECT * FROM tracks WHERE GenreId=9";
        List<String> playlists = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                playlists.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println(playlists);
    }

    @Test
    void insert(){
        String query = "INSERT INTO Playlists(Name) VALUES(?)";
        String playlist = "FOLK";
        try (Connection conn = DriverManager.getConnection(dbUrl)) {
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, playlist);

            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(getAllPlaylists());
    }
    @Test
    void iHateJazz(){
        deletePlaylist("Jazz");
        System.out.println(getAllPlaylists());
    }
    @Test
    void update() {
        String query = "UPDATE Playlists SET Name=? WHERE PlaylistId=?";
        try (Connection conn = DriverManager.getConnection(dbUrl)) {
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, "deathmetal");
            ps.setInt(2, 1);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(getAllPlaylists());
    }
}