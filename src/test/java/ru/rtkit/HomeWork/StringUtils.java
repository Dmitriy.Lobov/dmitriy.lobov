package ru.rtkit.HomeWork;

public class StringUtils {
    public static boolean isPalindrome(String str) {
        boolean a;
        a = str.equals(
                new StringBuilder(str)
                        .reverse()
                        .toString()
        );
        return a;
    }
    public static int divide(int a, int b) { return a/b; }
}
