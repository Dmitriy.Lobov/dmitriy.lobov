package ru.rtkit.HomeWork;

import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@DisplayName("Логгирование")
public class SomeClass {
    public static Logger LOGGER = LoggerFactory.getLogger(SomeClass.class);
    @Test
    @DisplayName("Пример логгирования")
    @Description("Используем 2 метода логгирования")
    @Attachment
    public void LogExample() {
        LOGGER.info("Это сообщение будет выведено в лог");
        LOGGER.error("Здесь будет лог с ошибкой");
    }
}