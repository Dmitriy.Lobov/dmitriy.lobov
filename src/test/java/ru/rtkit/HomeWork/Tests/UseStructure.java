package ru.rtkit.HomeWork.Tests;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import ru.rtkit.HomeWork.apiHelper.Endpoints;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UseStructure extends BaseTest {

    @Test
    @DisplayName("Провека наличия питомца")
    @Description("Добавление нового питомца и проверка его наличия в базе")
    void findPetByID() {

        String petName = "Vlozhik";
        JSONObject bodyJO = new JSONObject()
                .put("id", "361")
                .put("сategory", new JSONObject().put("name", "cat"))
                .put("name", petName)
                .put("status", "available");

        long id = addNewPet(bodyJO.toString(), petName);
        String actualName = apiHelper.get(Endpoints.PET_ID, resp200, id)
                .jsonPath()
                .getString("name");
        Assertions.assertEquals(petName, actualName, "Ошибка при проверке имени созданного животного");
    }

    @Step("Добавление питомца {petNames} в магазин ")
    long addNewPet(String pet, String petNames) {

        return apiHelper.post(Endpoints.NEW_PET, pet, resp200)
                .jsonPath()
                .getLong("id");
    }

    //==================================================================================================================

    @Test
    @DisplayName("Наличие питомца в магазине")
    @Description("Добавление питомца и проверка его наличия в магазине")
    void checkPetStore() {

        JSONObject bodyJO = new JSONObject()
                .put("name", "Вложик");
        long id = addNewPet(bodyJO.toString(), "Вложик");
        JSONObject bodyNextJO = new JSONObject()
                .put("id", 0)
                .put("petId", id)
                .put("quantity", 0)
                .put("shipDate", "2022-07-27T16:20:15.497Z")
                .put("status", "placed")
                .put("complete", true);

        Boolean complete = apiHelper.post(Endpoints.ORDER, bodyNextJO.toString(),resp200)
                .jsonPath()
                .getBoolean("complete");
        assertTrue(complete == true);
    }

    //==================================================================================================================

    static Stream<String> names() {

        return Stream.of("Busya", "BUSYA", "busya", "BuSyA", "Буся", "БУСЯ", "буся", "БуСя");
    }

    @ParameterizedTest
    @MethodSource("names")
    @DisplayName("Проверка имен")
    @Description("Проверка возможности добавления имени в разных регистрах")
    void addNewName(String argument) {

        JSONObject bodyJO = new JSONObject()
                .put("name", argument);
        long id = addNewPet(bodyJO.toString(), argument);
        String newName = apiHelper.get(Endpoints.PET_ID, resp200, id)
                .jsonPath()
                .getString("name");
        assertThat(argument, Matchers.equalTo(newName));
    }

}

