package ru.rtkit.HomeWork.Tests.HW7;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.rtkit.HomeWork.apiHelper.*;

import java.io.FileInputStream;
import java.io.IOException;

import static ru.rtkit.HomeWork.Tests.BaseTest.*;

public class ClassObject {
    @Test
    @DisplayName("CreateStoreOrder")
    void CheckStoreOrder() {
        StoreOrder newOrder = new StoreOrder();
        newOrder.setId(7);
        newOrder.setPetId(361);
        newOrder.setQuantity(4);
        newOrder.setShipDate("2022-08-14T10:27:56.204Z");
        newOrder.setStatus("placed");
        newOrder.setComplete(true);

        ApiHelper.post(Endpoints.ORDER, newOrder, resp200);
        StoreOrder OrderActual = ApiHelper.get(Endpoints.ORDER_ID, resp200, newOrder.getId()).as(StoreOrder.class);
        System.out.println(OrderActual.getPetId());
        Assertions.assertEquals(OrderActual.getId(), OrderActual.getId());
    }

    @Test
    void workWithProperties() {
        System.out.println(props);
        System.out.println(props.getProperty("serviceName"));
        Assumptions.assumeTrue(props.getProperty("serviceName").equals("petstore"));
        System.out.println(props.getProperty("serviceName"));
    }
    }
