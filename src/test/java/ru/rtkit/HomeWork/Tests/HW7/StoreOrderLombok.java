package ru.rtkit.HomeWork.Tests.HW7;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StoreOrderLombok {
    Integer id;
    Integer petId;
    Integer quantity;
    String shipDate;
    String status;
    Boolean complete;
}
